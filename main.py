
from flask import flash
from flask import *
from DBHelper import *
from functools import wraps
import mammoth
import csv
import os
import shutil
import random

app = Flask("app")
app.secret_key = 'secretkey'

#--------------------EVENT PHOTO FOLDER------------------------------------------

UPLOAD_FOLDER = './static/events'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

#--------------------EVENT PHOTO FOLDER------------------------------------------

def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'user' in session and session["user"]:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('sign'))
    return wrap

@app.route("/launch")
def launch():
	return render_template("launch.html")

@app.route("/")
def main():
	with open("./static/thirukural.csv","r") as csvf:
		read = csv.DictReader(csvf);
		read = list(read);
		i = random.randint(0,1080);
		k = read[i]["Verse"];
		k = k.split('\t');
		thiru = ""
		kural = ""
		cnt = 0
		print(len(k))
		for i in k:
			if i != " ":
				cnt+=1
				if cnt<=4:
					thiru = thiru + i.strip(" ") + " ";
				else:
					kural = kural + i.strip(" ") + " ";
		f = open("kavithai.docx","r");
		f = f.readlines()
		content = f
		f = open("art.txt","r");
		art = f.read();
		f.close()
		return render_template("home.html",kural=kural,thiru=thiru,kavithai=content,art = art);

##################################################################################################
@app.route("/event")
def eve():
    pastevent=event.pastevents()
    upcomingevent=event.upcomingevents()
    return render_template("events.html",pastevent=pastevent,upcomingevent=upcomingevent);

##################################################################################################
@app.route("/members")
def mem():
	members  = me.getallmembers()
	mem = []
	i = 0
	n = len(members)
	while i<n:
		temp = []
		l = min(4,n-i)
		for j in range(0,l):
			temp.append(members[i])
			i = i + 1
		mem.append(temp)
	return render_template("members.html",members = mem);

@app.route("/articles")
def article():
	articles = ar.getallarticles()
	return render_template("articles.html",articles=articles)

@app.route("/displayarticle")
def display():
	aid = request.args.get("aid");
	article = ar.getarticle(aid);
	f = open("./articles/" + article["aid"] + ".docx", 'rb')
	document = mammoth.convert_to_html(f)
	content = document.value
	f.close()
	return render_template("articledisplay.html",article=article,content=content)


@app.route("/books")
def books():
	books = bo.getallbooks()
	return render_template("books.html",books=books)

@app.route("/gallery")
def gallery():
    return render_template("photogallery.html")

@app.route("/about")
def about():
    return render_template("about.html")
#---------------------------------  ADMIN ACCESS  ------------------------
#---------------------------------  ADMIN ACCESS  ------------------------

@app.route("/admin",methods=["GET","POST"])
def sign():
	if(request.method == "POST"):
		k,mess = us.signin(request.form["uid"],request.form["password"]);
		if not k:
			return render_template("signin.html",message = mess);
		else:
			session["user"] = k
	if "user" in session.keys() and session["user"]:
		uid = request.args.get("uid")
		users=us.getallusers()
		edituser = None
		for one in users:
			if uid and one["uid"] == uid:
				edituser = one
		return render_template("admin_home.html",users=users,edituser=edituser,curr = session["user"]);
	else:
		return render_template("signin.html");

@app.route("/adminedit",methods=["GET","POST"])
@is_logged_in
def edituser():
	if(request.method == "POST"):
		us.adduser(request.form["uid"],request.form["password"],request.form["level"])
		if request.form["uid"] == session["user"]["uid"]:
			session["user"] = None;
		return redirect(url_for("sign"));

@app.route("/admin/removeuser")
@is_logged_in
def removeadmin():
	uid = request.args.get("uid")
	us.removeuser(uid)
	return redirect(url_for("sign"))

@app.route("/admin/kavithai",methods=["GET","POST"])
@is_logged_in
def kavithaiadd():
	if(request.method == "POST"):
		kavi = request.form["kavithai"]
		print(kavi)
		fil = open("kavithai.docx","w");
		fil.write(kavi)
		fil.close()
		return redirect(url_for("sign"))

@app.route("/admin/art",methods=["GET","POST"])
@is_logged_in
def artadd():
	if(request.method == "POST"):
		kavi = request.files["photo"]
		f = open("art.txt","w")
		print(kavi)
		f.write(kavi.filename)
		f.close();
		kavi.save("static/" + kavi.filename);
		return redirect(url_for("sign"))

#--------------------ADMIN ARTICLE------------------------------------------

@app.route("/admin/article")
@is_logged_in
def adminarticle():
	aid = request.args.get("aid");
	articles = ar.getallarticles()
	editauthor = None
	editname = None
	for one in articles:
		if aid and one["aid"] == aid:
			editauthor = one["author"]
			editname = one["name"]
	if not aid:
			aid = len(articles) + 1
	return render_template("admin_article.html",articles=articles,curr=session["user"],aid=aid,editauthor=editauthor,editname=editname);

@app.route("/articleedit",methods=["GET","POST"])
#@is_logged_in
def editarticle():
	if(request.method == "POST"):
		ar.addarticle(request.form["aid"],request.form["author"],request.form["name"])
		if request.form.get("filech"):
			file = request.files["content"];
			file.save("./articles/" + request.form["aid"] + ".docx");
		return redirect(url_for("adminarticle"));

@app.route("/admin/removearticle")
@is_logged_in
def removearticle():
	aid = request.args.get("aid")
	ar.removearticle(aid)
	return redirect(url_for("adminarticle"))


#--------------------ADMIN LIBRARY------------------------------------------

@app.route("/admin/library")
@is_logged_in
def adminlibrary():
	bkid = request.args.get("bkid");
	books = bo.getallbooks()
	editbook = None
	for one in books:
		if bkid and one["bkid"] == bkid:
			editbook = one
	if not bkid:
			bkid = len(books) + 1
	return render_template("admin_library.html",books=books,curr=session["user"],bkid=bkid,editbook=editbook);

@app.route("/bookedit",methods=["GET","POST"])
@is_logged_in
def editbook():
	if(request.method == "POST"):
		bo.addbook(request.form["bkid"],request.form["author"],request.form["name"])
		return redirect(url_for("adminlibrary"));

@app.route("/admin/removebook")
@is_logged_in
def bookremove():
	bkid = request.args.get("bkid")
	bo.removebook(bkid)
	return redirect(url_for("adminlibrary"))

@app.route("/admin/lendbook",methods=["GET","POST"])
@is_logged_in
def lendbook():
	if(request.method == "POST"):
		bo.lendbook(request.form["bkid"],request.form["rname"],request.form["rphone"],request.form["rid"],request.form["due"])
		return redirect(url_for("adminlibrary"));
	bkid = request.args.get("bkid")
	return render_template("admin_lend.html",bkid=bkid)

@app.route("/admin/returnbook")
@is_logged_in
def returmbook():
	bkid = request.args.get("bkid")
	bo.returnbook(bkid)
	return redirect(url_for("adminlibrary"))

@app.route("/admin/defaulters")
def defaulters():
	defa = bo.listdefaulters()
	return render_template("admin_defaulters.html",defaulters=defa)

#-------------------ADMIN MEMBER------------------------------------------


@app.route("/admin/members")
@is_logged_in
def adminmember():
	aid = request.args.get("mid");
	members = me.getallmembers()
	editmember = None
	for one in members:
		if aid and one["mid"] == aid:
			editmember = one
	return render_template("admin_member.html",members=members,curr=session["user"],editmember=editmember);

@app.route("/memberedit",methods=["GET","POST"])
@is_logged_in
def editmember():
	if(request.method == "POST"):
		file = None
		if("photo" in request.files):
			file = request.files["photo"];
		fname = None;
		if(file != None):
			n = file.filename
			n = n.split(".")
			n = n[-1]
			file.save("./static/members/" + request.form["mid"] + "." + n);
			fname = request.form["mid"] + "." + n;
			fname = request.form["mid"] + "." + n;
		me.addmember(request.form["mid"],request.form["name"],request.form["phone"],request.form["department"],request.form["post"],fname)
		return redirect(url_for("adminmember"));

@app.route("/admin/removemember")
@is_logged_in
def removemember():
	aid = request.args.get("mid")
	m = me.getmember(aid)
	fname = m["filename"]
	os.remove("./static/members/" + fname)
	me.removemember(aid)
	return redirect(url_for("adminmember"))



# -----------------------ADMIN ENDS-----------------------------

# -----------------------EVENT STARTS-----------------------------

@app.route("/admin/addevent",methods=["GET","POST"])
#@is_logged_in
def add_event():
    events=event.listofevents()
    max=0
    for i in events:
        if(int(i["eid"])>max):
            max=int(i["eid"])
    max=max+1
    if(request.method=="POST"):
        photo = request.files['photo']
        eid=request.form["eid"]
        filename =photo.filename
        #os.mkdir("static/events/"+eid)
        #photo.save(os.path.join(app.config['UPLOAD_FOLDER'],eid,filename))
        event.addevent(request.form["eid"],filename,request.form["name"],request.form["content"],request.form["date"],request.form["venue"],request.form["time"])
        flash('updated Successfully')
    return render_template("addevents.html",eid=max,events=events)

@app.route("/displayevent")
def displayevent():
    eid = request.args.get("eid");
    events = event.retrieveevent(eid);
    return render_template("displayevent.html",events=events)

@app.route("/pastevent")
def pastevent():
    eid=request.args.get("eid");
    gallerypics=gal.getgallerypics(eid)
    contents=gal.getcontent(eid)
    details=event.retrieveevent(eid)
    #name=details["name"]
    return render_template("pastevent.html",gallerypics=gallerypics,contents=contents,details=details)

@app.route("/upcomingevent")
def upcomingevent():
    eid=request.args.get("eid")
    print(eid,eid)
    events=event.retrieveevent(eid)
    print(events)
    return render_template("upcomingevent.html",events=events)

@app.route("/admin/removeevent",methods=["GET","POST"])
def removesevent():
    eid=request.args.get("eid")
    print(eid)
    try:
        event.removeevent(eid)
        #shutil.rmtree(os.path.join(app.config['UPLOAD_FOLDER'],eid))
        return "Requested event has been removed"
    except FileNotFoundError:
        return "File already deleted"

@app.route("/admin/updateevent",methods=["GET","POST"])
def updateevent():
    if(request.method=='GET'):
        eid=request.args.get("eid")
        events=event.retrieveevent(eid)
        return render_template("updateevent.html",event=events)
    if(request.method=='POST'):
        photo = request.files['photo']
        eid=request.form["eid"]
        filename =photo.filename
        list=event.retrieveevent(eid)
        #os.remove(os.path.join(app.config['UPLOAD_FOLDER'],eid,list["photo"]))
        #photo.save(os.path.join(app.config['UPLOAD_FOLDER'],eid,filename))
        event.addevent(request.form["eid"],filename,request.form["name"],request.form["content"],request.form["date"],request.form["venue"],request.form["time"])
        return render_template("addevents.html")

@app.route("/admin/uploadgallery",methods=["GET","POST"])
def uploadgallery():
    if(request.method=='GET'):
        eid=request.args.get("eid")
        return render_template("gallery.html",eid=eid)
    if(request.method=='POST'):
        eid=request.form["eid"]
        flag=gal.retrieveevent(eid)
        if flag:
            oldgallery=gal.getgallerypics(eid)
            for pic in oldgallery:
                print("yessss")
                #os.remove(os.path.join(app.config['UPLOAD_FOLDER'],pic["eid"],pic["photo"]))
        photo = request.files.getlist('photo')
        list=event.retrieveevent(eid)
        ###os.remove(os.path.join(app.config['UPLOAD_FOLDER'],eid,list["photo"]))
        if photo:
            photobucket=[]
            for i in photo:
                filename =i.filename
                print (filename)
                #i.save(os.path.join(app.config['UPLOAD_FOLDER'],eid,filename))
                photobucket.append(filename)
            gal.addgallery(request.form["eid"],photobucket,request.form["content"])
        return "Gallery Uploaded/Updated"

# -----------------------EVENT ENDS-----------------------------


if __name__ == '__main__':
    port = int(os.environ.get('PORT'))
    app.run(host='0.0.0.0', port=port)
    app.secret_key = 'secretkey'
    app.run(debug=True)
