import pymongo
import bcrypt
from datetime import *
#testing
#MONGO_URL='localhost:27017'
#deployment
MONGO_URL='mongodb://cibi:cibicool@ds147681.mlab.com:47681/mlib'
#print(MONGO_URL)
client=pymongo.MongoClient(MONGO_URL)
db=client.mlib
book = db.book
user = db.user
article = db.article
member = db.member
eve=db.event
gallery=db.gallery
pasteventcontent=db.pasteventcontent
eve.create_index([("eid",pymongo.ASCENDING)],unique=True)
book.create_index([("bkid",pymongo.ASCENDING)],unique=True)
user.create_index([("uid",pymongo.ASCENDING)],unique=True)

class us:
	def signin(uid,passw):
		u = user.find_one({"uid":uid});
		if(u):
			if passw == u["password"]:
				return {"uid":u["uid"],"level":u["level"]},"none"
			else:
				return None,"incorrect password"
		else:
			return None,"invalid username"

	def adduser(uid,passw,le):
		u = user.find_one({"uid":uid});
		if(u):
			user.update({"uid":uid},{"uid":uid,"password":passw,"level":le});
		else:
			user.insert({"uid":uid,"password":passw,"level":le})

	def removeuser(uid):
		user.remove({"uid":uid});

	def getallusers():
		u = user.find();
		kp = []
		for one in u:
			kp.append({"uid":one["uid"],"level":one["level"],"password":one["password"]})
		return kp;

class ar:
	def addarticle(aid,author,name):
		u = article.find_one({"aid":aid});
		if(u):
			article.update({"aid":aid},{"aid":aid,"author":author,"name":name});
		else:
			article.insert({"aid":aid,"author":author,"name":name})

	def removearticle(aid):
		article.remove({"aid":aid});

	def getallarticles():
		u = article.find();
		kp = []
		for one in u:
			kp.append({"aid":one["aid"],"author":one["author"],"name":one["name"]})
		return kp;

	def getarticle(aid):
		u = article.find_one({"aid":aid});
		return u



class bo:
	def addbook(bkid,author,name):
		u = book.find_one({"bkid":bkid});
		if(u):
			book.update({"bkid":bkid},{"$set":{"bkid":bkid,"author":author,"name":name}});
		else:
			book.insert({"bkid":bkid,"author":author,"name":name,"availability":1})

	def removebook(bkid):
		book.remove({"bkid":bkid});

	def getallbooks():
		u = book.find();
		kp = []
		for one in u:
			one["author"] = ""
			kp.append({"bkid":one["bkid"],"author":one["author"],"name":one["name"],"availability":one["availability"]})
		return kp;

	def lendbook(bkid,rname,rphone,rid,due):
		book.update({"bkid":bkid},{"$set":{"availability":0,"rname":rname,"rphone":rphone,"rid":rid,"due":due}})

	def returnbook(bkid):
		u = book.find_one({"bkid":bkid})
		author = u["author"]
		name = u["name"]
		book.update({"bkid":bkid},{"bkid":bkid,"author":author,"name":name,"availability":1})


	def listdefaulters():
		noav = book.find({"availability":0})
		defaulters = []
		for bookd in noav:
			d = datetime.now()
			ld = bookd["due"]
			due = datetime.strptime(ld,"%Y-%m-%d")
			ld = due + timedelta(days=15)
			print(ld,d)
			if ld <= d :
				defaulters.append({"name":bookd["name"],"rname":bookd["rname"],"rid":bookd["rid"],"rphone":bookd["rphone"]})
		return defaulters


class me:
	def addmember(mid,name,phone,department,post,filename):
		u = member.find_one({"mid":mid});
		if(u):
			if not filename:
				filename = u["filename"]
			member.update({"mid":mid},{"mid":mid,"name":name,"phone":phone,"department":department,"post":post,"filename":filename});
		else:
			if not filename:
				return
			member.insert({"mid":mid,"name":name,"phone":phone,"department":department,"post":post,"filename":filename})

	def removemember(mid):
		member.remove({"mid":mid});

	def getallmembers():
		u = member.find();
		kp = []
		for one in u:
			kp.append({"mid":one["mid"],"name":one["name"],"phone":one["phone"],"department":one["department"],"post":one["post"],"filename":one["filename"]})
		return kp;

	def getmember(mid):
		u = member.find_one({"mid":mid});
		return u

###################################EVENT STARTS##########################################
class event:
	def addevent(eid,photo,name,content,date,venue,time):
		e = eve.find_one({"eid":eid});
		if(e):
			eve.update({"eid":eid},{"eid":eid,"photo":photo,"name":name,"content":content,"date":date,"venue":venue,"time":time})
		else:
			eve.insert({"eid":eid,"photo":photo,"name":name,"content":content,"date":date,"venue":venue,"time":time})

	def listofevents():
		list = eve.find();
		eventlist = []
		for one in list:
			if "eid" in one:
				eventlist.append({"eid":one["eid"],"photo":one["photo"],"name":one["name"],"content":one["content"],"date":one["date"],"venue":one["venue"],"time":one["time"]})
		return eventlist;

	def retrieveevent(eid):
		e = eve.find_one({"eid":eid});
		return e

	def removeevent(eid):
		print(eid)
		if eve.find_one({"eid":eid}):
			eve.remove({"eid":eid})
		if gallery.find_one({"eid":eid}):
			gallery.remove({"eid":eid})
		if pasteventcontent.find_one({"eid":eid}):
			pasteventcontent.remove({"eid":eid})
		return None

	def pastevents():
		list=eve.find()
		d = datetime.today()
		eventlist=[]
		for one in list:
			if "eid" in one:
				eventdate = one["date"]
				eventdate = datetime.strptime(eventdate,"%Y-%m-%d")
				if eventdate.date() < d.date() :
					eventlist.append({"eid":one["eid"],"photo":one["photo"],"name":one["name"],"content":one["content"],"date":one["date"],"venue":one["venue"],"time":one["time"]})
		return eventlist

	def upcomingevents():
		list=eve.find()
		d = datetime.today()
		eventlist=[]
		for one in list:
			if "date" in one:
				eventdate = one["date"]
				eventdate = datetime.strptime(eventdate,"%Y-%m-%d")
				if eventdate.date() >= d.date() :#should change
					eventlist.append({"eid":one["eid"],"photo":one["photo"],"name":one["name"],"content":one["content"],"date":one["date"],"venue":one["venue"],"time":one["time"]})
		return eventlist

class gal:
	def addgallery(eid,photo,content):
		g = gallery.find_one({"eid":eid})
		if(g):
			db.gallery.remove({"eid":eid})
			db.pasteventcontent.remove({"eid":eid})
		for i in photo:
			gallery.insert({"eid":eid,"photo":i})
		pasteventcontent.insert({"eid":eid,"content":content})

	def getgallerypics(eid):
		list=gallery.find({"eid":eid})
		gallerypics=[]
		for one in list:
			gallerypics.append({"eid":one["eid"],"photo":one["photo"]})
		return gallerypics

	def getcontent(eid):
		two=pasteventcontent.find_one({"eid":eid})
		list=[]
		list.append({"eid":eid,"content":two["content"]})
		return list

	def retrieveevent(eid):
		e = pasteventcontent.find_one({"eid":eid});
		return e


#gallery.remove()
#pasteventcontent.remove()
#event.remove()
